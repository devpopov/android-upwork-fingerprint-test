package com.upwork.androidupworkfingerprinttest.views.interfaces;

/**
 * Created by sergey on 02.04.16.
 */
public interface IFingerprintAuthActivity {
    void success(String result);
}
