package com.upwork.androidupworkfingerprinttest.views;

import android.app.DialogFragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.upwork.androidupworkfingerprinttest.R;
import com.upwork.androidupworkfingerprinttest.presenters.FingerprintDialogPresenter;
import com.upwork.androidupworkfingerprinttest.views.interfaces.IFingerprintAuthActivity;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import nucleus.factory.RequiresPresenter;

/**
 * Created by sergey on 02.04.16.
 */

@RequiresPresenter(FingerprintDialogPresenter.class)
public class FingerprintDialog extends NucleusDialogFragment<FingerprintDialogPresenter> {

    private static final long ERROR_TIMEOUT_MILLIS = 1600;
    private static final long SUCCESS_DELAY_MILLIS = 1300;

    public static final String FINGERPRINT_DIALOG_TAG = "fingerprint";

    @Bind(R.id.fingerprint_icon)
    ImageView fingerprintIcon;

    @Bind(R.id.fingerprint_status)
    TextView fingerprintStatus;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setRetainInstance(true);
        setStyle(DialogFragment.STYLE_NORMAL, android.R.style.Theme_Material_Light_Dialog);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        getDialog().setTitle(getString(R.string.sign_in));
        View view = inflater.inflate(R.layout.fingerprint_dialog_container, container, false);

        ButterKnife.bind(this, view);

        return view;
    }

    @OnClick(R.id.cancel_button)
    public void cancel() {
        getPresenter().stopListening();

        dismiss();
    }

    public void error(String error) {
        getPresenter().stopListening();

        this.fingerprintIcon.setImageResource(R.drawable.ic_fingerprint_error);
        this.fingerprintStatus.setText(error);
        this.fingerprintStatus.setTextColor(
                this.fingerprintStatus.getResources().getColor(R.color.warning_color, null));

        this.fingerprintIcon.postDelayed(new Runnable() {
            @Override
            public void run() {
                cancel();
            }
        }, ERROR_TIMEOUT_MILLIS);
    }

    public void success(final String result) {
        this.fingerprintIcon.setImageResource(R.drawable.ic_fingerprint_success);

        this.fingerprintStatus.setTextColor(this.fingerprintStatus.getResources().getColor(R.color.success_color, null));
        this.fingerprintStatus.setText(
                this.fingerprintStatus.getResources().getString(R.string.fingerprint_success));

        this.fingerprintIcon.postDelayed(new Runnable() {
            @Override
            public void run() {
                cancel();
                // return to parent activity result of encryption / decryption
                ((IFingerprintAuthActivity)getActivity()).success(result);
            }
        }, SUCCESS_DELAY_MILLIS);
    }

    public void setMode(int mode) {
        getPresenter().setMode(mode);
    }

    public void setValue(String value) {
        getPresenter().setValue(value);
    }
}
