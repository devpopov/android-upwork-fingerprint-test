package com.upwork.androidupworkfingerprinttest.views;

import android.os.Bundle;
import android.text.method.PasswordTransformationMethod;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Toast;

import com.upwork.androidupworkfingerprinttest.R;
import com.upwork.androidupworkfingerprinttest.presenters.PasswordPresenter;
import com.upwork.androidupworkfingerprinttest.views.interfaces.IFingerprintAuthActivity;

import javax.crypto.Cipher;
import javax.inject.Inject;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnCheckedChanged;
import butterknife.OnClick;
import nucleus.factory.RequiresPresenter;
import nucleus.view.NucleusAppCompatActivity;

@RequiresPresenter(PasswordPresenter.class)
public class PasswordActivity extends NucleusAppCompatActivity<PasswordPresenter> implements IFingerprintAuthActivity {

    @Bind(R.id.password_field)
    EditText passwordField;

    @Bind(R.id.toggle_password_visibility_box)
    CheckBox togglePasswordVisibilityBox;

    @Inject
    FingerprintDialog fingerprintDialog;

    private FragmentsComponent fragmentsComponent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_password);

        ButterKnife.bind(this);

        this.fragmentsComponent = DaggerFragmentsComponent.builder()
                .fragmentsModule(new FragmentsModule(getApplicationContext()))
                .build();
        this.fragmentsComponent.inject(this);

        String password = getIntent().getStringExtra("password");

        if(password != null)
            passwordField.setText(password);
    }

    @OnCheckedChanged(R.id.toggle_password_visibility_box)
    public void togglePasswordVisibility() {
        if(this.togglePasswordVisibilityBox.isChecked()) {
            passwordField.setTransformationMethod(null);
            passwordField.setSelection(passwordField.getText().length());
        }
        else {
            passwordField.setTransformationMethod(new PasswordTransformationMethod());
            passwordField.setSelection(passwordField.getText().length());
        }
    }

    @OnClick(R.id.save_button)
    public void save() {
        getPresenter().onSave(passwordField.getText().toString());
    }

    @Override
    public void success(String result) {
        getPresenter().savePassword(result);
    }

    public void showFingerprintDialog(String value) {
        this.fingerprintDialog.setMode(Cipher.ENCRYPT_MODE);
        this.fingerprintDialog.setValue(value);

        this.fingerprintDialog.show(getFragmentManager(), FingerprintDialog.FINGERPRINT_DIALOG_TAG);
    }

    public void showMessage(String message) {
        Toast.makeText(this, message, Toast.LENGTH_LONG).show();
    }
}
