package com.upwork.androidupworkfingerprinttest.views;

import android.content.Context;

import dagger.Module;
import dagger.Provides;

/**
 * Created by sergey on 02.04.16.
 */
@Module
public class FragmentsModule {
    private final Context context;


    public FragmentsModule(Context context) {
        this.context = context;
    }

    @Provides
    public FingerprintDialog providesFingerprintDialog() {
        return new FingerprintDialog();
    }
}
