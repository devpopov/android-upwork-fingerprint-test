package com.upwork.androidupworkfingerprinttest.views;

import android.content.Intent;
import android.os.Bundle;
import android.widget.Toast;

import com.upwork.androidupworkfingerprinttest.R;
import com.upwork.androidupworkfingerprinttest.presenters.TapActivityPresenter;
import com.upwork.androidupworkfingerprinttest.views.interfaces.IFingerprintAuthActivity;

import javax.crypto.Cipher;
import javax.inject.Inject;

import butterknife.ButterKnife;
import butterknife.OnClick;
import nucleus.factory.RequiresPresenter;
import nucleus.view.NucleusAppCompatActivity;

@RequiresPresenter(TapActivityPresenter.class)
public class TapActivity extends NucleusAppCompatActivity<TapActivityPresenter> implements IFingerprintAuthActivity {
    private FragmentsComponent fragmentsComponent;

    @Inject
    FingerprintDialog fingerprintDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tap);

        ButterKnife.bind(this);

        this.fragmentsComponent = DaggerFragmentsComponent.builder()
                .fragmentsModule(new FragmentsModule(getApplicationContext()))
                .build();
        this.fragmentsComponent.inject(this);
    }

    @OnClick(R.id.tap_space)
    public void needFingerprint() {
        getPresenter().onNeedFingerprint();
    }

    public void showMessage(String message) {
        Toast.makeText(this, message, Toast.LENGTH_LONG).show();
    }

    public void showFingerprintDialog(String value, int mode) {
        this.fingerprintDialog.setMode(mode);
        this.fingerprintDialog.setValue(value);

        this.fingerprintDialog.show(getFragmentManager(), FingerprintDialog.FINGERPRINT_DIALOG_TAG);
    }

    @Override
    public void success(String result) {
        Intent intent = new Intent(TapActivity.this, PasswordActivity.class);
        intent.putExtra("password", result);

        // transfer decrypted password (or null if no password in shared preferences) to password activity
        startActivity(intent);
    }
}
