package com.upwork.androidupworkfingerprinttest.views;

import com.upwork.androidupworkfingerprinttest.core.fingerprint.FingerprintModule;

import dagger.Component;

/**
 * Created by sergey on 02.04.16.
 */

@Component(
        modules = {
                FragmentsModule.class
        }
)

public interface FragmentsComponent {
    void inject(TapActivity activity);
    void inject(PasswordActivity activity);
}
