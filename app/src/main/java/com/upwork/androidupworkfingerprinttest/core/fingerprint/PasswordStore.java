package com.upwork.androidupworkfingerprinttest.core.fingerprint;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by sergey on 02.04.16.
 */
public class PasswordStore {
    private final static String PASSWORD_KEY = "password";
    private final static String IV_KEY = "iv";

    private SharedPreferences preferences;

    public PasswordStore(Context context) {
        this.preferences = context.getSharedPreferences(context.getApplicationInfo().packageName, context.MODE_PRIVATE);
    }

    public void setIV(String iv) {
        SharedPreferences.Editor editor = this.preferences.edit();
        editor.putString(IV_KEY, iv);
        editor.commit();
    }

    public String getIV() {
        if(preferences.contains(IV_KEY)) {
            return preferences.getString(IV_KEY, null);
        }

        return null;
    }

    public void setPassword(String password) {
        SharedPreferences.Editor editor = this.preferences.edit();
        editor.putString(PASSWORD_KEY, password);
        editor.commit();
    }

    public String getPassword() {
        if(preferences.contains(PASSWORD_KEY)) {
            return preferences.getString(PASSWORD_KEY, null);
        }

        return null;
    }
}
