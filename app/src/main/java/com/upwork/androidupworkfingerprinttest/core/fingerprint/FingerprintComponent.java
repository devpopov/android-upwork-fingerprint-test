package com.upwork.androidupworkfingerprinttest.core.fingerprint;

import android.app.KeyguardManager;
import android.content.Context;
import android.content.SharedPreferences;
import android.hardware.fingerprint.FingerprintManager;

import com.upwork.androidupworkfingerprinttest.application.SingletonComponent;

import java.security.KeyStore;

import javax.crypto.Cipher;
import javax.crypto.KeyGenerator;

import dagger.Component;

/**
 * Created by sergey on 02.04.16.
 */

@Component(
        modules = {
                FingerprintModule.class
        }
)

public interface FingerprintComponent extends SingletonComponent {
        FingerprintManager fingerprintManager();
        KeyguardManager keyguardManager();
        KeyStore keystoreManager();
        KeyGenerator keyGenerator();
        Cipher cipher();
        PasswordStore store();
}
