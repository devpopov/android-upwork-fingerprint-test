package com.upwork.androidupworkfingerprinttest.application;

import javax.inject.Singleton;

import dagger.Component;

/**
 * Created by sergey on 01.04.16.
 */
@Singleton
@Component
public interface SingletonComponent {
}
