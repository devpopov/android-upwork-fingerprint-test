package com.upwork.androidupworkfingerprinttest.application;

import android.app.Application;

import com.upwork.androidupworkfingerprinttest.dagger.scopes.ApplicationScope;

import dagger.Module;
import dagger.Provides;

/**
 * Created by sergey on 01.04.16.
 */
@Module
public class AppModule {
    private Application application;

    public AppModule(Application application) {
        this.application = application;
    }

    @Provides
    @ApplicationScope
    public Application provideApplication() {
        return this.application;
    }
}
