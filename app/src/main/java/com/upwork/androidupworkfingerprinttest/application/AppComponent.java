package com.upwork.androidupworkfingerprinttest.application;

import com.upwork.androidupworkfingerprinttest.core.fingerprint.FingerprintComponent;
import com.upwork.androidupworkfingerprinttest.dagger.scopes.ApplicationScope;
import com.upwork.androidupworkfingerprinttest.presenters.FingerprintDialogPresenter;
import com.upwork.androidupworkfingerprinttest.presenters.PasswordPresenter;
import com.upwork.androidupworkfingerprinttest.presenters.TapActivityPresenter;

import dagger.Component;

/**
 * Created by sergey on 01.04.16.
 */

@ApplicationScope
@Component(
        dependencies = {
                FingerprintComponent.class
        },
        modules = {
                AppModule.class
        }
)

public interface AppComponent {
        void inject(TheApplication application);
        void inject(TapActivityPresenter presenter);
        void inject(FingerprintDialogPresenter presenter);
        void inject(PasswordPresenter presenter);
}