package com.upwork.androidupworkfingerprinttest.application;

import android.app.Application;
import android.content.Context;

import com.upwork.androidupworkfingerprinttest.core.fingerprint.DaggerFingerprintComponent;
import com.upwork.androidupworkfingerprinttest.core.fingerprint.FingerprintComponent;
import com.upwork.androidupworkfingerprinttest.core.fingerprint.FingerprintModule;

/**
 * Created by sergey on 01.04.16.
 */
public class TheApplication extends Application {
    private AppComponent appComponent;

    public static TheApplication get(Context context) {
        return (TheApplication) context.getApplicationContext();
    }

    @Override
    public void onCreate() {
        super.onCreate();
        initComponent();
    }

    public void initComponent() {
        FingerprintComponent fingerprintComponent = DaggerFingerprintComponent.builder()
                .fingerprintModule(new FingerprintModule(getApplicationContext()))
                .build();

        this.appComponent = DaggerAppComponent.builder()
                .fingerprintComponent(fingerprintComponent)
                .appModule(new AppModule(this))
                .build();
        this.appComponent.inject(this);
    }

    public AppComponent getAppComponent() {
        return this.appComponent;
    }
}