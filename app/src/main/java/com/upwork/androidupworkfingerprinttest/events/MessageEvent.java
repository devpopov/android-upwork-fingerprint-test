package com.upwork.androidupworkfingerprinttest.events;

/**
 * Created by sergey on 02.04.16.
 */
public class MessageEvent {
    public final String message;

    public MessageEvent(String message) {
        this.message = message;
    }
}
