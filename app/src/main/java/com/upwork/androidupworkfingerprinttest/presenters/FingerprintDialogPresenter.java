package com.upwork.androidupworkfingerprinttest.presenters;

import android.Manifest;
import android.content.pm.PackageManager;
import android.hardware.fingerprint.FingerprintManager;
import android.os.Bundle;
import android.os.CancellationSignal;
import android.security.keystore.KeyGenParameterSpec;
import android.security.keystore.KeyProperties;
import android.util.Base64;

import com.upwork.androidupworkfingerprinttest.R;
import com.upwork.androidupworkfingerprinttest.application.TheApplication;
import com.upwork.androidupworkfingerprinttest.core.fingerprint.PasswordStore;
import com.upwork.androidupworkfingerprinttest.events.MessageEvent;
import com.upwork.androidupworkfingerprinttest.views.FingerprintDialog;

import java.io.IOException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.UnrecoverableKeyException;
import java.security.cert.CertificateException;
import java.security.spec.InvalidParameterSpecException;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.KeyGenerator;
import javax.crypto.SecretKey;
import javax.crypto.spec.IvParameterSpec;
import javax.inject.Inject;

import de.greenrobot.event.EventBus;
import nucleus.presenter.RxPresenter;

/**
 * Created by sergey on 02.04.16.
 */
public class FingerprintDialogPresenter extends RxPresenter<FingerprintDialog> {
    private boolean selfCancelled;

    // secret key alias
    private String key;

    // value to be encrypted / decrypted
    private String value;

    private CancellationSignal cancellationSignal;

    private FingerprintManager.AuthenticationCallback authenticationCallback;

    private int mode;

    @Inject
    FingerprintManager fingerprintManager;

    @Inject
    KeyStore keyStore;

    @Inject
    KeyGenerator keyGenerator;

    @Inject
    Cipher cipher;

    @Inject
    PasswordStore store;

    @Override
    protected void onCreate(Bundle savedState) {
        super.onCreate(savedState);
    }

    @Override
    protected void onTakeView(FingerprintDialog view) {
        super.onTakeView(view);

        TheApplication.get(view.getContext()).getAppComponent().inject(this);

        this.key = getView().getContext().getApplicationInfo().packageName + "_FINGERPRINT_KEY";

        startListening();
    }

    private void createKey() {
        try {
            this.keyStore.load(null);

            // check if alias already in keystore

            if (!this.keyStore.containsAlias(this.key)) {
                this.keyGenerator.init(new KeyGenParameterSpec.Builder(this.key,
                        KeyProperties.PURPOSE_ENCRYPT |
                                KeyProperties.PURPOSE_DECRYPT)
                        .setBlockModes(KeyProperties.BLOCK_MODE_CBC)
                        .setUserAuthenticationRequired(true)
                        .setEncryptionPaddings(KeyProperties.ENCRYPTION_PADDING_PKCS7)
                        .build());
                this.keyGenerator.generateKey();
            }
        } catch (NoSuchAlgorithmException | InvalidAlgorithmParameterException
                | CertificateException | IOException | KeyStoreException e) {
            throw new RuntimeException("Failed to get key", e);
        }
    }

    private void initCipher() {
        try {
            // get secret key by alias
            SecretKey secretKey = (SecretKey) this.keyStore.getKey(this.key, null);
            IvParameterSpec ivParams;

            // check needed cipher mode
            if (this.mode == Cipher.ENCRYPT_MODE) {
                this.cipher.init(this.mode, secretKey);
                ivParams = this.cipher.getParameters().getParameterSpec(IvParameterSpec.class);

                // save iv in shared preferences for future use in decrypt mode
                store.setIV(Base64.encodeToString(ivParams.getIV(), Base64.DEFAULT));
            } else {
                // init iv stored in shared preferences
                ivParams = new IvParameterSpec(Base64.decode(store.getIV(), Base64.DEFAULT));
                this.cipher.init(mode, secretKey, ivParams);
            }
        } catch (NoSuchAlgorithmException | UnrecoverableKeyException | InvalidKeyException | InvalidAlgorithmParameterException | KeyStoreException | InvalidParameterSpecException e) {
            throw new RuntimeException("Failed to init Cipher", e);
        }
    }

    private void startListening() {
        createKey();
        initCipher();

        if (getView().getContext().checkSelfPermission(Manifest.permission.USE_FINGERPRINT) != PackageManager.PERMISSION_GRANTED) {
            EventBus.getDefault().post(new MessageEvent(getView().getResources().getString(R.string.no_fingerprint_permission)));
            return;
        }

        cancellationSignal = new CancellationSignal();

        authenticationCallback = new FingerprintManager.AuthenticationCallback() {
            @Override
            public void onAuthenticationError(int errorCode, CharSequence errString) {
                super.onAuthenticationError(errorCode, errString);
                if (!selfCancelled)
                    getView().error(errString.toString());
            }

            @Override
            public void onAuthenticationHelp(int helpCode, CharSequence helpString) {
                super.onAuthenticationHelp(helpCode, helpString);

                getView().error(helpString.toString());
            }

            @Override
            public void onAuthenticationSucceeded(FingerprintManager.AuthenticationResult result) {
                super.onAuthenticationSucceeded(result);

                // check if value exists
                if (value != null) {
                    try {
                        // check cipher mode
                        if (mode == Cipher.DECRYPT_MODE) {

                            // just decode value and return it to view
                            String output = new String(cipher.doFinal(Base64.decode(value, Base64.DEFAULT)));
                            getView().success(output);
                        }
                        else if (mode == Cipher.ENCRYPT_MODE) {

                            // just encode value and return it to view
                            String output = Base64.encodeToString(cipher.doFinal(value.getBytes()), Base64.DEFAULT);
                            getView().success(output);
                        }
                    } catch (IllegalBlockSizeException | BadPaddingException e) {
                        throw new RuntimeException("Failed to do final operation in cipher", e);
                    }
                } else {
                    // just return null to view
                    getView().success(value);
                }
            }

            @Override
            public void onAuthenticationFailed() {
                super.onAuthenticationFailed();

                getView().error(getView().getString(R.string.auth_failed));
            }
        };

        this.selfCancelled = false;

        fingerprintManager.authenticate(new FingerprintManager.CryptoObject(this.cipher), this.cancellationSignal, 0, this.authenticationCallback, null);
    }

    public void stopListening() {
        if (this.cancellationSignal != null) {
            this.cancellationSignal.cancel();
            this.cancellationSignal = null;
            this.selfCancelled = true;
        }
    }

    public void setMode(int mode) {
        this.mode = mode;
    }

    public void setValue(String value) {
        this.value = value;
    }
}
