package com.upwork.androidupworkfingerprinttest.presenters;

import android.Manifest;
import android.app.KeyguardManager;
import android.content.pm.PackageManager;
import android.hardware.fingerprint.FingerprintManager;
import android.os.Bundle;

import com.upwork.androidupworkfingerprinttest.R;
import com.upwork.androidupworkfingerprinttest.application.TheApplication;
import com.upwork.androidupworkfingerprinttest.core.fingerprint.PasswordStore;
import com.upwork.androidupworkfingerprinttest.events.MessageEvent;
import com.upwork.androidupworkfingerprinttest.views.TapActivity;

import javax.crypto.Cipher;
import javax.inject.Inject;

import de.greenrobot.event.EventBus;
import nucleus.presenter.RxPresenter;

/**
 * Created by sergey on 01.04.16.
 */
public class TapActivityPresenter extends RxPresenter<TapActivity> {
    @Inject
    KeyguardManager keyguardManager;

    @Inject
    FingerprintManager fingerprintManager;

    @Inject
    PasswordStore store;

    @Override
    protected void onCreate(Bundle savedState) {
        super.onCreate(savedState);

        EventBus.getDefault().register(this);
    }

    @Override
    protected void onTakeView(TapActivity view) {
        super.onTakeView(view);

        TheApplication.get(getView()).getAppComponent().inject(this);
    }

    public void onNeedFingerprint() {
        if (!this.keyguardManager.isKeyguardSecure()) {
            getView().showMessage(getView().getResources().getString(R.string.need_screen_lock));
            return;
        }

        if (getView().checkSelfPermission(Manifest.permission.USE_FINGERPRINT) != PackageManager.PERMISSION_GRANTED) {
            getView().showMessage(getView().getResources().getString(R.string.no_fingerprint_permission));
            return;
        }

        if (!this.fingerprintManager.hasEnrolledFingerprints()) {
            getView().showMessage(getView().getResources().getString(R.string.no_fingerprint));
            return;
        }

        String password = store.getPassword();

        if(password != null) {
            getView().showFingerprintDialog(store.getPassword(), Cipher.DECRYPT_MODE);
        }
        else {
            // if no password stored (first time running) just authorize and generate secret key
            getView().showFingerprintDialog(store.getPassword(), Cipher.ENCRYPT_MODE);
        }
    }

    public void onEvent(MessageEvent event) {
        getView().showMessage(event.message);
    }
}
