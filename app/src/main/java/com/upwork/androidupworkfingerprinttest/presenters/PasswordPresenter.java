package com.upwork.androidupworkfingerprinttest.presenters;

import android.os.Bundle;

import com.upwork.androidupworkfingerprinttest.R;
import com.upwork.androidupworkfingerprinttest.application.TheApplication;
import com.upwork.androidupworkfingerprinttest.core.fingerprint.PasswordStore;
import com.upwork.androidupworkfingerprinttest.views.PasswordActivity;

import javax.crypto.Cipher;
import javax.inject.Inject;

import nucleus.presenter.RxPresenter;

/**
 * Created by sergey on 02.04.16.
 */
public class PasswordPresenter extends RxPresenter<PasswordActivity> {
    @Inject
    Cipher cipher;

    @Inject
    PasswordStore store;

    @Override
    protected void onCreate(Bundle savedState) {
        super.onCreate(savedState);
    }

    @Override
    protected void onTakeView(PasswordActivity view) {
        super.onTakeView(view);

        TheApplication.get(getView()).getAppComponent().inject(this);
    }

    public void onSave(String password) {
        if(password != "")
        {
            getView().showFingerprintDialog(password);
        }
    }

    public void savePassword(String password) {
        store.setPassword(password);

        getView().showMessage(getView().getString(R.string.password_saved));
    }
}
